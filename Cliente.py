#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Estamos en Pi1
#Nos limitamos a enviar lo que se mande con una pulsacion de teclado
#o mediante el teclado cuando pulsemos Intro
#Mostramos lo que nos mandan de Pi2 en consola y en LCD.
host = "192.168.1.132"
port = 8040
import socket
import drivers
display = drivers.Lcd()
from pad4pi import rpi_gpio
import time
from datetime import datetime
import RPi.GPIO as GPIO
clave=""
estadoPuerta=""
def long_string(tupla):
    display=tupla[0]
    texto1=tupla[1]
    texto2=tupla[2]
    #display.lcd_clear()
    #display.lcd_backlight(1) 
    num_cols=16
    print("Texto1 "+texto1+" en linea 1")
    print("Texto2 "+texto2+" en linea 2")
    display.lcd_display_string(texto1, 1)
    if len(texto2) > num_cols:
        display.lcd_display_string(texto2[:num_cols], 2)
        time.sleep(0.5)
        for i in range(len(texto2) - num_cols + 1):
            text_to_print = texto2[i:i+num_cols]
            display.lcd_display_string(text_to_print, 2)
            time.sleep(0.2)
        time.sleep(0.5)
    else:
        display.lcd_display_string(texto2, 2)
    if "apagada" in texto2:
        display.lcd_backlight(0)
def aclararTexto(texto):
    texto=texto[2:len(texto)-1]
    return texto
def enviarOrden(orden):
    global estadoPuerta
    global clave
    obj.send(orden.encode('ascii'))
    time.sleep(0.2)
    recibido=aclararTexto(str(obj.recv(1024)))
    if recibido=="Abierta" or recibido=="Cerrada":
        clave=""
        estadoPuerta=recibido
        MyT = display, "-Puerta "+estadoPuerta+"-", "-Inserte clave: "
    elif "*" in recibido:
        clave=clave+recibido
        MyT = display, "-Puerta "+estadoPuerta+"-", "Inserte clave: "+clave
    elif "Auten" in recibido:
        clave=""
        MyT = display, "-Puerta "+estadoPuerta+"-", "-Clave Erronea-"
    elif recibido=="Limpiar":
        clave=""
        MyT = display, "-Puerta "+estadoPuerta+"-", "-Inserte clave:"
    elif "Apagar" in recibido:
        clave=""
        MyT = display, "-Puerta "+estadoPuerta+"-", "-Luz apagada-"
    else:
        MyT = display, "-Puerta "+estadoPuerta+"-", "Desconectado de Pi2"
        clave=""
    return MyT
def key_pressed(key):
    long_string(enviarOrden(key))

KEYPAD = [
                        ["1","2","3","A"],
                        ["4","5","6","B"],
                        ["7","8","9","C"],
                        ["*","0","#","D"]
                ]

GPIO.setmode(GPIO.BCM)
COL_PINS = [13,6,5,0]           # set up BCM GPIO numbering
ROW_PINS = [21,20,26,19]  # BCM GPIO numbering
factory = rpi_gpio.KeypadFactory()
keypad = factory.create_keypad(keypad=KEYPAD, row_pins=ROW_PINS, col_pins=COL_PINS)
keypad.registerKeyPressHandler(key_pressed)
#Creacion de un objeto socket (lado cliente)
obj = socket.socket()
#Conexion con el servidor.
obj.connect((host, port))
print("Conectado a Pi2")
#Deberemos preguntar como esta la puerta a Pi2:
long_string(enviarOrden("Estado"))
while True:
    time.sleep(0.5)
    long_string(enviarOrden(input("Teclear Abrir/Cerrar: ")))
#Instanciamos una entrada de datos para que el cliente pueda enviarlos
#Cerramos la instancia del objeto servidor
obj.close()
print("Conexion cerrada")
keypad.cleanup()
time.sleep(1)
