
#include <Servo.h>               // Gracias a :
Servo servoMotor;               //Creamos objeto de la clase Servo
String inputString = "";       // Cadena para guardar el comando recibido
bool stringComplete = false;  // Bandera boleana que nos indica cuando el comando fue recibido y podemos compararlo con los 2 comandos válidos
void setup() {
  Serial.begin(9600);          //Arrancamos la comunicacion por Serial
  inputString.reserve(200);   //Reservamos el espacio.
  servoMotor.attach(3);    //Especificamos por que PIN se daran ordenes al servomotor.        
}
void loop() { 
                                         //Recibimos las instrucciones desde la Pi2: "Abrir" o "Cerrar".
 if (stringComplete) {                  //El comando fue recibido, procedemos a compararlo    
    if (inputString.equals("Abrir") ){ //Si el comando es "Abrir"
      abrirPuerta(15);
    }
    else if (inputString.equals("Cerrar") ){ //Si el comando es "Cerrar"
      cerrarPuerta(15);
    }
    inputString = "";         // Limpiamos la cadena para poder recibir el siguiente comando
    stringComplete = false;  // Bajamos la bandera para no volver a ingresar a la comparación hasta que recibamos un nuevo comando
  } 
}
void serialEvent() { 
  while (Serial.available()) {         //Mientras tengamos caracteres disponibles en el buffer
    char inChar = (char)Serial.read();//Leemos el siguiente caracter
    if (inChar == '\n') {            //Si el caracter recibido corresponde a un salto de línea
      stringComplete = true;        //Levantamos la bandera 
    }
    else{                    //Si el caracter recibido no corresponde a un salto de línea
      inputString += inChar;//Agregamos el caracter a la cadena 
    }
  }
}
void abrirPuerta(int dep){
      for (int pos = 0; pos <= 90; pos += 1) { 
      servoMotor.write(pos);              //Mandamos señal para colocar el motor.
      delay(dep);                       
      }
      Serial.println("Abierta");  //Mandamos a pi2 el estadoPuerta "Abierta"
}
void cerrarPuerta(int dep){ 
      for (int pos = 90; pos >= 0; pos -= 1) { 
      servoMotor.write(pos);              
      delay(dep);                      
      } 
      Serial.println("Cerrada"); //Mandamos a pi2 el estadoPuerta "Cerrada"
}